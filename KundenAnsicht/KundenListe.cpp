#include "KundenListe.h"

KundenListe::KundenListe(QWidget *parent) : QListWidget(parent)
{
    setAlternatingRowColors(true);
    update();
    connect(this, SIGNAL(currentRowChanged(int)), this, SLOT(auswahl(int)));
    connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem *)),
            this, SLOT(aktiviert(QListWidgetItem *)));
}

void KundenListe::auswahl(int r)
{
    emit kundeGeaendert(liste.at(r));
}

void KundenListe::aktiviert(QListWidgetItem * )
{
    KundenDialog dlg(liste.at(currentRow()),this);
    dlg.exec();
    update(currentRow());
}

void KundenListe::update(int r)
{
	disconnect(this, SIGNAL(currentRowChanged(int)), this, SLOT(auswahl(int)));
    clear();
    liste.clear();
    liste = Datenbank::alleKunden();
    int elem;
    foreach(elem,liste)
    {
        Kunde k = Datenbank::kunde(elem);
        QString str;
        if (k.firma != "")
            str += k.firma + " - ";
        str += k.vorname + " ";
        str += k.name;
        addItem(str);
    }
    setCurrentRow(r);
	connect(this, SIGNAL(currentRowChanged(int)), this, SLOT(auswahl(int)));
}

void KundenListe::start()
{
	if (count())
		auswahl(0);
}
