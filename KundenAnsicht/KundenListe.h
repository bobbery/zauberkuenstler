#ifndef KUNDENLISTE
#define KUNDENLISTE

#include <QtGui>
#include <QtCore>
#include "Datenbank/Datenbank.h"
#include "KundenDialog/KundenDialog.h"

class KundenListe : public QListWidget
{
	Q_OBJECT

    private slots:
        void auswahl(int r);
        void aktiviert(QListWidgetItem *);

    public slots:
        void update(int r=0);

    public:
        KundenListe(QWidget *parent = 0);
        t_kundenListe liste;
		void start();

    protected:

    private:

    signals:
        void kundeGeaendert(int kdnr);
};

#endif


