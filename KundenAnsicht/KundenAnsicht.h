#ifndef KUNDENANSICHT
#define KUNDENANSICHT

#include "Datenbank/Datenbank.h"
#include <QtGui>
#include <QTextBrowser>

class KundenAnsicht : public QTextBrowser
{
	Q_OBJECT

    private slots:

    public slots:
        void kundeGeaendert(int kdnr);

    public:
        KundenAnsicht(QWidget *parent = 0);



};

#endif

