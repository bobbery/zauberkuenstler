#include "AuftrittListe.h"

AuftrittListe::AuftrittListe(QWidget *parent) : QListWidget(parent)
{
    kundennummer = 0;
    setAlternatingRowColors(true);
    setMaximumHeight(100);
    update();
    connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem *)),
            this, SLOT(aktiviert(QListWidgetItem *)));
}

void AuftrittListe::aktiviert(QListWidgetItem *)
{
    AuftrittDialog dlg(liste.at(currentRow()),this);
    dlg.exec();
    update(currentRow());
}

void AuftrittListe::update(int r)
{
    clear();
    liste.clear();
    if (kundennummer == 0) return;
    liste = Datenbank::kundenAuftritte(kundennummer);
    int elem;
    foreach(elem,liste)
    {
        Auftritt a = Datenbank::auftritt(elem);
        QString str;
        str += "Am " + a.datum.toString("dd.MM.yyyy");
        str += " um " + a.uhrzeit.toString("hh:mm") + " Uhr";
        addItem(str);
    }
    setCurrentRow(r);
}

void AuftrittListe::setKundennummer(int kdnr)
{
        kundennummer = kdnr;
        update();
}

