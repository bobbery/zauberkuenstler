#include "KundenAnsicht.h"

KundenAnsicht::KundenAnsicht(QWidget *parent) : QTextBrowser(parent)
{
	//setFixedWidth(500);
}

void KundenAnsicht::kundeGeaendert(int kdnr)
{
    Kunde k = Datenbank::kunde(kdnr);
    QString html = Datenbank::generiereHtml(ARBEITSVERZ + "vorlagen/kunde.zk",k);
    setHtml(html);
}

