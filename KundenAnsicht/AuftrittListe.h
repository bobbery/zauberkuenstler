#ifndef AUFTRITTLISTE
#define AUFTRITTLISTE

#include <QtGui>
#include <QtCore>
#include "Datenbank/Datenbank.h"
#include "AuftrittDialog/AuftrittDialog.h"

class AuftrittListe : public QListWidget
{
	Q_OBJECT

    private slots:
        void aktiviert(QListWidgetItem * i);

    public slots:
        void update(int r = 0);
        void setKundennummer(int kdnr);

    public:
        AuftrittListe(QWidget *parent = 0);
        t_auftrittListe liste;

    protected:

    private:
        int kundennummer;

};

#endif


