#include "Notizen.h"

Notizen::Notizen(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);
}

void Notizen::getData(Auftritt &a)
{
    a.vereinbarung = ui.textVereinbarung->toPlainText();
    a.anfahrt = ui.textAnfahrt->toPlainText();
}


void Notizen::setData(const Auftritt &a)
{
    ui.textVereinbarung->setPlainText(a.vereinbarung);
    ui.textAnfahrt->setPlainText(a.anfahrt);
}


