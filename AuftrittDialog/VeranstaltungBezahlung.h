#ifndef VERANSTALTUNGBEZAHLUNG
#define VERANSTALTUNGBEZAHLUNG

#include "ui_VeranstaltungBezahlung.h"
#include "Datenbank/Datenbank.h"


typedef QList<QCheckBox *> t_boxListe;

class VeranstaltungBezahlung : public QWidget
{
	Q_OBJECT

    private slots:
        void chgGage(double wert);
		void chgFahrt(double wert);
		void chgSonstiges(double wert);
		void chgGesamt(double wert);
		void chgMwst(double wert);

    public slots:

    public:
        VeranstaltungBezahlung(QWidget *parent = 0);
        void getData(Auftritt &a);
        void setData(const Auftritt &a);

    private:
        QList<QCheckBox*> * checkboxen;
        Ui::VeranstaltungBezahlung ui;
};

#endif



