#include "VeranstaltungBezahlung.h"

VeranstaltungBezahlung::VeranstaltungBezahlung(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);

	checkboxen = new QList<QCheckBox*>;
    checkboxen->append(ui.cb1);
    checkboxen->append(ui.cb2);
    checkboxen->append(ui.cb3);
    checkboxen->append(ui.cb4);
    checkboxen->append(ui.cb5);
    checkboxen->append(ui.cb6);
    checkboxen->append(ui.cb7);
    checkboxen->append(ui.cb8);
    checkboxen->append(ui.cb9);
    checkboxen->append(ui.cb10);
    checkboxen->append(ui.cb11);
    checkboxen->append(ui.cb12);

	connect(ui.dblGage, SIGNAL(valueChanged(double)), this, SLOT(chgGage(double)));
	connect(ui.dblFahrt, SIGNAL(valueChanged(double)), this, SLOT(chgFahrt(double)));
	connect(ui.dblSonstiges, SIGNAL(valueChanged(double)), this, SLOT(chgSonstiges(double)));
	connect(ui.dblGesamt, SIGNAL(valueChanged(double)), this, SLOT(chgGesamt(double)));
	connect(ui.dblMwst, SIGNAL(valueChanged(double)), this, SLOT(chgMwst(double)));
}

void VeranstaltungBezahlung::getData(Auftritt &a)
{
    QString str;
    QCheckBox * box;

    foreach (box, *checkboxen)
    {
        if (box->checkState()) str += box->text() + ", ";
    }
    str.chop(2);

    a.veranstaltungsart = str;

    a.gage = ui.dblGage->value();
    a.fahrtgeld = ui.dblFahrt->value();
    a.sonstiges = ui.dblSonstiges->value();
}

void VeranstaltungBezahlung::setData(const Auftritt &a)
{
    QCheckBox * box;
    foreach (box, *checkboxen)
    {
        if (a.veranstaltungsart.contains(box->text()))
            box->setCheckState(Qt::Checked);
    }
    ui.dblGage->setValue(a.gage);
    ui.dblFahrt->setValue(a.fahrtgeld);
    ui.dblSonstiges->setValue(a.sonstiges);
    ui.dblGesamt->setValue(a.gage+a.fahrtgeld+a.sonstiges);
    ui.dblMwst->setValue(ui.dblGesamt->value() / 100.0 * (100.0 + MWST));

}

void VeranstaltungBezahlung::chgGage(double wert)
{
	if (!ui.dblGage->hasFocus()) return;
	wert = ui.dblGage->value() + ui.dblFahrt->value() + ui.dblSonstiges->value();
	ui.dblGesamt->setValue(wert);
	ui.dblMwst->setValue((wert / 100.0) * (100.0+MWST));
}

void VeranstaltungBezahlung::chgFahrt(double wert)
{
	if (!ui.dblFahrt->hasFocus()) return;
	wert = ui.dblGage->value() + ui.dblFahrt->value() + ui.dblSonstiges->value();
	ui.dblGesamt->setValue(wert);
	ui.dblMwst->setValue((wert / 100.0) * (100.0+MWST));
}

void VeranstaltungBezahlung::chgSonstiges(double wert)
{
	if (!ui.dblSonstiges->hasFocus()) return;
	wert = ui.dblGage->value() + ui.dblFahrt->value() + ui.dblSonstiges->value();
	ui.dblGesamt->setValue(wert);
	ui.dblMwst->setValue((wert / 100.0) * (100.0+MWST));
}

void VeranstaltungBezahlung::chgGesamt(double wert)
{
	if (!ui.dblGesamt->hasFocus()) return;
	wert = ui.dblGesamt->value();
	ui.dblGage->setValue(wert);
	ui.dblFahrt->setValue(0.0);
	ui.dblSonstiges->setValue(0.0);
	ui.dblMwst->setValue((wert / 100.0) * (100.0+MWST));
}

void VeranstaltungBezahlung::chgMwst(double wert)
{
	if (!ui.dblMwst->hasFocus()) return;
	wert = (wert / (100.0+MWST)) * 100.0;
	ui.dblGesamt->setValue(wert);
	ui.dblGage->setValue(wert);
	ui.dblFahrt->setValue(0.0);
	ui.dblSonstiges->setValue(0.0);
}


