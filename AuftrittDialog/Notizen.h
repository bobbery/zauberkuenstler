#ifndef NOTIZEN
#define NOTIZEN

#include "ui_Notizen.h"
#include "Datenbank/Datenbank.h"

class Notizen : public QWidget
{
	Q_OBJECT

    private slots:

    public slots:

    public:
        Notizen(QWidget *parent = 0);
        void getData(Auftritt &a);
        void setData(const Auftritt &a);

    private:
        Ui::Notizen ui;
};

#endif

