#include "OrtDatum.h"

OrtDatum::OrtDatum(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);
	ui.dateDatum->setDate(QDate::currentDate());
    ui.dateAuftragseingang->setDate(QDate::currentDate());
}

void OrtDatum::getData(Auftritt &a)
{
    a.auftragseingang = ui.dateAuftragseingang->date();
    a.datum = ui.dateDatum->date();
    a.uhrzeit = ui.timeUhrzeit->time();
    a.dauer = ui.cmbDauer->currentText();
    a.eintreffen = ui.cmbFrueher->currentText();
    a.ort_name = ui.leOrtName->displayText();
    a.ort_strasse = ui.leOrtStrasse->displayText();
    a.ort_plz = ui.leOrtPlz->displayText();
    a.ort_ort = ui.leOrtOrt->displayText();
    a.melden_bei = ui.leMelden->displayText();
    if (ui.cbUeberraschung->checkState())
        a.ueberraschung = true;
    else
        a.ueberraschung = false;
}

void OrtDatum::setData(const Auftritt &a)
{
    ui.dateAuftragseingang->setDate(a.auftragseingang);
    ui.dateDatum->setDate(a.datum);
    ui.timeUhrzeit->setTime(a.uhrzeit);

    ui.cmbDauer->setCurrentIndex(ui.cmbDauer->findText(a.dauer));
    ui.cmbFrueher->setCurrentIndex(ui.cmbFrueher->findText(a.eintreffen));

    ui.leOrtName->setText(a.ort_name);
    ui.leOrtStrasse->setText(a.ort_strasse);
    ui.leOrtPlz->setText(a.ort_plz);
    ui.leOrtOrt->setText(a.ort_ort);
    ui.leMelden->setText(a.melden_bei);
    if (a.ueberraschung)
        ui.cbUeberraschung->setCheckState(Qt::Checked);
    else
        ui.cbUeberraschung->setCheckState(Qt::Unchecked);
}

void OrtDatum::editierbar(bool b)
{
    ui.dateAuftragseingang->setEnabled(b);
}

void OrtDatum::setDatum(const QDate &d)
{
    ui.dateDatum->setDate(d);
	//ui.dateDatum->setEnabled(false);
}

