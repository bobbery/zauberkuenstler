#ifndef ORTDATUM
#define ORTDATUM

#include "ui_OrtDatum.h"
#include "Datenbank/Datenbank.h"

class OrtDatum : public QWidget
{
	Q_OBJECT

    private slots:

    public slots:

    public:
        OrtDatum(QWidget *parent = 0);
        void getData(Auftritt &a);
        void setData(const Auftritt &a);
        void editierbar(bool b);
        void setDatum(const QDate &d);

    private:
        Ui::OrtDatum ui;
};

#endif

