#ifndef AUFTRITTDIALOG
#define AUFTRITTDIALOG

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QToolBox>
#include <QListWidget>
#include <QLabel>
#include "KundenDialog/KundenMaske.h"
#include "Datenbank/Datenbank.h"
#include "VeranstaltungBezahlung.h"
#include "OrtDatum.h"
#include "Notizen.h"
#include "KundenAuswahl.h"
#include "DruckDialog/DruckDialog.h"

class AuftrittDialog : public QDialog
{
	Q_OBJECT

    private slots:
        void anlegen();
        void aendern();
        void loeschen();
        void drucken();

    public slots:

    public:
        AuftrittDialog(int auftrittnummer, QWidget *parent = 0);
        AuftrittDialog(const QDate &d, QWidget *parent = 0);

    private:
        Auftritt a;
        VeranstaltungBezahlung * veranstbezahl;
        OrtDatum * ortdatum;
        Notizen * notizen;
        QToolBox * toolbox;
        QVBoxLayout * topLayout;
        KundenMaske * kundenmaske;
        QHBoxLayout * buttonLayout;
        QPushButton * btnSpeichern;
        QPushButton * btnAbbrechen;
        QPushButton * btnLoeschen;
        QPushButton * btnDrucken;
};

#endif



