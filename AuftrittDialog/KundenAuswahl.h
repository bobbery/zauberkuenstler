#ifndef KUNDENAUSWAHL
#define KUNDENAUSWAHL

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QListWidget>
#include <QLabel>
#include "Datenbank/Datenbank.h"

class KundenAuswahl : public QDialog
{
	Q_OBJECT

    private slots:
        void auswahl();
        void neuKunde();
        void abbrechen();

    public slots:

    public:
        KundenAuswahl(t_kundenListe &l, QWidget *parent = 0);

    private:
        t_kundenListe liste;
        QVBoxLayout * layout;
        QLabel * label;
        QListWidget * list;
        QPushButton * btnAuswahl;
        QPushButton * btnNeukunde;
        QPushButton * btnAbbruch;

};

#endif


