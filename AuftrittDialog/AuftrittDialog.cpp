#include "AuftrittDialog.h"

AuftrittDialog::AuftrittDialog(const QDate &d, QWidget *parent) : QDialog(parent)
{
    resize(800,600);
    setWindowTitle("Neuen Auftritt anlegen");
    topLayout = new QVBoxLayout(this);
    toolbox = new QToolBox(this);
    kundenmaske = new KundenMaske(this);
    veranstbezahl = new VeranstaltungBezahlung(this);
    ortdatum = new OrtDatum(this);
    ortdatum->setDatum(d);
    notizen = new Notizen(this);
    toolbox->addItem(ortdatum,"Ort/Datum");
    toolbox->addItem(kundenmaske,"Kundendaten");
    toolbox->addItem(veranstbezahl,"Veranstaltungsart / Bezahlung");
    toolbox->addItem(notizen,"Notizen");
    buttonLayout = new QHBoxLayout(this);
    btnSpeichern = new QPushButton("Auftritt anlegen");
    btnAbbrechen = new QPushButton("Abbrechen");
    topLayout->addWidget(toolbox);
    topLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(btnAbbrechen);
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnSpeichern);

    connect(btnSpeichern, SIGNAL(clicked()), this, SLOT(anlegen()));
    connect(btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
}

AuftrittDialog::AuftrittDialog(int auftrittnummer, QWidget *parent) : QDialog(parent)
{
    resize(800,600);
    topLayout = new QVBoxLayout(this);
    toolbox = new QToolBox(this);
    kundenmaske = new KundenMaske(this);
    veranstbezahl = new VeranstaltungBezahlung(this);
    ortdatum = new OrtDatum(this);
    notizen = new Notizen(this);
    toolbox->addItem(ortdatum,"Ort/Datum");
    toolbox->addItem(kundenmaske,"Kundendaten");
    toolbox->addItem(veranstbezahl,"Veranstaltungsart / Bezahlung");
    toolbox->addItem(notizen,"Notizen");
    buttonLayout = new QHBoxLayout(this);
    btnSpeichern = new QPushButton(QLatin1String("Auftritt �ndern"));
    btnLoeschen = new QPushButton(QLatin1String("Auftritt l�schen"));
    btnAbbrechen = new QPushButton("Abbrechen");
    btnDrucken = new QPushButton("Drucken");
    topLayout->addWidget(toolbox);
    topLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(btnAbbrechen);
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnSpeichern);
    buttonLayout->addWidget(btnLoeschen);
    buttonLayout->addWidget(btnDrucken);

    kundenmaske->editierbar(false);
    ortdatum->editierbar(false);

    a = Datenbank::auftritt(auftrittnummer);
    QString titel = "Auftritt ";
    titel += a.auftragseingang.toString();
    titel += "/";
    titel += QString::number(a.auftrittnummer);
    setWindowTitle(titel);
    ortdatum->setData(a);
    veranstbezahl->setData(a);
    notizen->setData(a);
    kundenmaske->setKunde(a.kunde);

    connect(btnSpeichern, SIGNAL(clicked()), this, SLOT(aendern()));
    connect(btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
    connect(btnLoeschen, SIGNAL(clicked()), this, SLOT(loeschen()));
    connect(btnDrucken, SIGNAL(clicked()), this, SLOT(drucken()));

}


void AuftrittDialog::anlegen()
{
        Kunde k;
        k = kundenmaske->getKunde();
        t_kundenListe Liste = Datenbank::existiert(k);
        if (!Liste.count())
        {
            // keine �hnlichen Kunden in DB
            Datenbank::erzeugen(k);
        }
        else
        {
            // �hnliche Kunden gefunden -> Liste anzeigen
            KundenAuswahl auswahl(Liste, this);
            int result = auswahl.exec();
            if (result == 0)
                Datenbank::erzeugen(k);
            else if (result < 0)
                return;
            else
                k.kundennummer = result;
        }
        a.kunde = Datenbank::kunde(k.kundennummer);
        ortdatum->getData(a);
        veranstbezahl->getData(a);
        notizen->getData(a);
        Datenbank::erzeugen(a);
        close();
}

void AuftrittDialog::aendern()
{
        ortdatum->getData(a);
        veranstbezahl->getData(a);
        notizen->getData(a);
        Datenbank::aendern(a);
        close();
}

void AuftrittDialog::drucken()
{
    t_auftrittListe Liste;
    Liste.append(a.auftrittnummer);
    DruckDialog dlg(Liste, DruckDialog::dlgAuftritt, this);
    dlg.exec();
}

void AuftrittDialog::loeschen()
{
        int reply =QMessageBox::warning(this,  QLatin1String("Auftritt l�schen"),
                                QLatin1String("wollen Sie diesen Auftritt wirklich l�schen ?"),
                                QMessageBox::No | QMessageBox::Yes);

        if (reply == QMessageBox::Yes)
        {
            Datenbank::loeschen(a);
        }
        close();
}


