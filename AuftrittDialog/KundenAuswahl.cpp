#include "KundenAuswahl.h"

KundenAuswahl::KundenAuswahl(t_kundenListe &l, QWidget *parent) : QDialog(parent)
{
    resize(300,300);
    liste = l;
    setWindowTitle("Neuen Auftritt anlegen");
    int elem;
    layout = new QVBoxLayout(this);
    list = new QListWidget(this);
    list->setAlternatingRowColors(true);
    btnAuswahl = new QPushButton("Diesen Kunden nehmen");
    btnNeukunde = new QPushButton("Neuen Kunden anlegen");
    btnAbbruch = new QPushButton("Zur�ck");
    label = new QLabel( "Es wurden �hnliche Kunden in der Datenbank gefunden.\n"
                        "W�hle bitte einen Kunden aus der Liste oder "
                        "klicke auf 'Neuen Kunden anlegen'.");
    layout->addWidget(label);
    layout->addWidget(list);
    layout->addWidget(btnAuswahl);
    layout->addWidget(btnNeukunde);
    layout->addWidget(btnAbbruch);

    foreach(elem,liste)
    {
        Kunde k = Datenbank::kunde(elem);
        QString str;
        if (k.firma != "")
            str += k.firma + " - ";
        str += k.vorname + " ";
        str += k.name + ", ";
        str += k.strasse + ", ";
        str += k.plz + " ";
        str += k.wohnort;
        list->addItem(str);
    }

    list->setCurrentRow(0);

    connect(btnAuswahl, SIGNAL(clicked()), this, SLOT(auswahl()));
    connect(btnNeukunde, SIGNAL(clicked()), this, SLOT(neuKunde()));
    connect(btnAbbruch, SIGNAL(clicked()), this, SLOT(abbrechen()));
}

void KundenAuswahl::auswahl()
{
    done(liste.at(list->currentRow()));
}

void KundenAuswahl::neuKunde()
{
    done(0);
}

void KundenAuswahl::abbrechen()
{
    done(-1);
}
