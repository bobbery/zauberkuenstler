#include "Agenda.h"

Agenda::Agenda(QWidget *parent) : QTextBrowser(parent)
{
	// setFixedWidth(500);
    t_auftrittListe liste;
    datum = QDate::currentDate();
    datumGeaendert(datum);
    connect(this, SIGNAL(anchorClicked(const QUrl &)), this, SLOT(auftrittAngeklickt(const QUrl &)));
}

void Agenda::datumGeaendert(const QDate &d)
{
    datum = d;
    t_auftrittListe liste;
    liste = Datenbank::agenda(datum);
    int elem;
    QString html;
    foreach(elem,liste)
    {
        Auftritt a = Datenbank::auftritt(elem);
        html += Datenbank::generiereHtml(ARBEITSVERZ + "vorlagen/agenda.zk", a);
    }
    setHtml(html);
}

void Agenda::auftrittAngeklickt(const QUrl &u)
{
    AuftrittDialog dlg(u.toString().toInt(),this);
    dlg.exec();
    setSource(source());
    datumGeaendert(datum);
}


