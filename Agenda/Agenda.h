#ifndef AGENDA
#define AGENDA

#include "Datenbank/Datenbank.h"
#include "AuftrittDialog/AuftrittDialog.h"
#include <QtGui>
#include <QTextBrowser>

class Agenda : public QTextBrowser
{
	Q_OBJECT

    private slots:

    public slots:
        void datumGeaendert(const QDate &d);
        void auftrittAngeklickt(const QUrl &u);

    public:
        Agenda(QWidget *parent = 0);

    private:
        QDate datum;

};

#endif

