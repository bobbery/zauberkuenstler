######################################################################
# Automatically generated by qmake (3.0) So. Feb. 26 15:44:39 2017
######################################################################

TEMPLATE = app
TARGET = zauberkuenstler
INCLUDEPATH += .

QT += sql gui widgets printsupport

# Input
HEADERS += Agenda/Agenda.h \
           AuftrittDialog/AuftrittDialog.h \
           AuftrittDialog/KundenAuswahl.h \
           AuftrittDialog/Notizen.h \
           AuftrittDialog/OrtDatum.h \
           AuftrittDialog/VeranstaltungBezahlung.h \
           Datenbank/Datenbank.h \
           DruckDialog/DruckDialog.h \
           Hauptfenster/Hauptfenster.h \
           Kalender/Kalender.h \
           KundenAnsicht/AuftrittListe.h \
           KundenAnsicht/KundenAnsicht.h \
           KundenAnsicht/KundenListe.h \
           KundenDialog/KundenDialog.h \
           KundenDialog/KundenMaske.h \
           SerienDruck/SerienDruck.h
FORMS += AuftrittDialog/Notizen.ui \
         AuftrittDialog/OrtDatum.ui \
         AuftrittDialog/VeranstaltungBezahlung.ui \
         DruckDialog/DruckDialog.ui \
         Hauptfenster/Hauptfenster.ui \
         KundenDialog/KundenMaske.ui \
         SerienDruck/SerienDruck.ui
SOURCES += Agenda/Agenda.cpp \
           AuftrittDialog/AuftrittDialog.cpp \
           AuftrittDialog/KundenAuswahl.cpp \
           AuftrittDialog/Notizen.cpp \
           AuftrittDialog/OrtDatum.cpp \
           AuftrittDialog/VeranstaltungBezahlung.cpp \
           Datenbank/Datenbank.cpp \
           DruckDialog/DruckDialog.cpp \
           Hauptfenster/Hauptfenster.cpp \
           Kalender/Kalender.cpp \
           KundenAnsicht/AuftrittListe.cpp \
           KundenAnsicht/KundenAnsicht.cpp \
           KundenAnsicht/KundenListe.cpp \
           KundenDialog/KundenDialog.cpp \
           KundenDialog/KundenMaske.cpp \
           SerienDruck/SerienDruck.cpp \
           main.cpp

RC_ICONS = Puzzle.ico
