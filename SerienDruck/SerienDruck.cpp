#include "SerienDruck.h"

SerienDruck::SerienDruck(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);

   connect(ui.btnKunden, SIGNAL(clicked()), this, SLOT(kunden()));
   connect(ui.btnAuftritte, SIGNAL(clicked()), this, SLOT(auftritte()));
   connect(ui.btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
}

void SerienDruck::kunden()
{
    t_kundenListe Liste;
    QString filter;

    if (ui.cbFirma->checkState() == Qt::Checked)
        filter += "firma LIKE '" + ui.leFirma->text() + "' AND ";
    if (ui.cbVorname->checkState() == Qt::Checked)
        filter += "vorname LIKE '" + ui.leVorname->text() + "' AND ";
    if (ui.cbName->checkState() == Qt::Checked)
        filter += "name LIKE '" + ui.leName->text() + "' AND ";
    if (ui.cbStrasse->checkState() == Qt::Checked)
        filter += "strasse LIKE '" + ui.leStrasse->text() + "' AND ";
    if (ui.cbPlz->checkState() == Qt::Checked)
        filter += "plz LIKE '" + ui.lePlz->text() + "' AND ";
    if (ui.cbWohnort->checkState() == Qt::Checked)
        filter += "wohnort LIKE '" + ui.leWohnort->text() + "' AND ";
    filter.chop(4);

    Liste = Datenbank::alleKunden(filter);
    if (!Liste.count())
    {
        QMessageBox::about( this,"Meldung","Nichts gefunden");
        return;
    }
    DruckDialog dlg(Liste,DruckDialog::dlgKunde,this);
    dlg.exec();
}

void SerienDruck::auftritte()
{
    t_auftrittListe Liste;
    QString filter;

    if (ui.cbFirma->checkState() == Qt::Checked)
        filter += "firma LIKE '" + ui.leFirma->text() + "' AND ";
    if (ui.cbVorname->checkState() == Qt::Checked)
        filter += "vorname LIKE '" + ui.leVorname->text() + "' AND ";
    if (ui.cbName->checkState() == Qt::Checked)
        filter += "name LIKE '" + ui.leName->text() + "' AND ";
    if (ui.cbStrasse->checkState() == Qt::Checked)
        filter += "strasse LIKE '" + ui.leStrasse->text() + "' AND ";
    if (ui.cbPlz->checkState() == Qt::Checked)
        filter += "plz LIKE '" + ui.lePlz->text() + "' AND ";
    if (ui.cbWohnort->checkState() == Qt::Checked)
        filter += "wohnort LIKE '" + ui.leWohnort->text() + "' AND ";
    if (ui.cbAuftragseingang->checkState() == Qt::Checked)
    {
        filter  +=  "DATE(auftragseingang) >= DATE('"
                +   ui.dateEingangVon->date().toString("yyyy-MM-dd")
                + "') AND DATE(auftragseingang) <= DATE('"
                +   ui.dateEingangBis->date().toString("yyyy-MM-dd")
                + "') AND ";
    }

    if (ui.cbDatum->checkState() == Qt::Checked)
    {
        filter  +=  "DATE(datum) >= DATE('"
                +   ui.dateDatumVon->date().toString("yyyy-MM-dd")
                + "') AND DATE(datum) <= DATE('"
                +   ui.dateDatumBis->date().toString("yyyy-MM-dd")
                + "') AND ";
    }

    if (ui.cbGage->checkState() == Qt::Checked)
    {
        filter  +=  "gage >= "
                +   QString::number(ui.doubleGageVon->value(),'f',2)
                + " AND gage <= "
                +   QString::number(ui.doubleGageBis->value(),'f',2)
                + " AND ";
    }

    filter.chop(4);

    Liste = Datenbank::alleAuftritte(filter);
    if (!Liste.count())
    {
        QMessageBox::about( this,"Meldung","Nichts gefunden");
        return;
    }
    DruckDialog dlg(Liste,DruckDialog::dlgAuftritt,this);
    dlg.exec();
}


