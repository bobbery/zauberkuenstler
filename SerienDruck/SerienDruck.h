#ifndef SERIENDRUCK
#define SERIENDRUCK

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include "Datenbank/Datenbank.h"
#include "DruckDialog/DruckDialog.h"
#include "ui_SerienDruck.h"

class SerienDruck : public QDialog
{
	Q_OBJECT

    private slots:
        void kunden();
        void auftritte();

    public slots:

    public:
        SerienDruck(QWidget *parent = 0);

    private:
        Ui::SerienDruck ui;

};

#endif


