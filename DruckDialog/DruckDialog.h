#ifndef DRUCKDIALOG
#define DRUCKDIALOG

#include <QDialog>
#include <QtGui>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include "Datenbank/Datenbank.h"
#include "ui_DruckDialog.h"

class DruckDialog : public QDialog
{
	Q_OBJECT

    private slots:
        void vorlageGeaendert(int i);
        void weiter();
        void zurueck();
        void drucken();

    public slots:

    public:
        enum DialogTyp {dlgAuftritt =0 , dlgKunde = 1};
        DruckDialog(const QList<int> &l, DialogTyp t, QWidget *parent = 0);

    private:
        QString generiereHtml();
        QString getPfad();

        int sequenz;
        bool auftritt;  // mit kunden oder auftrittListe aufgerufen ?
        QList<int> liste;
        Ui::DruckDialog ui;
};

#endif


