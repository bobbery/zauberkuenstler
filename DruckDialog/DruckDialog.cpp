#include "DruckDialog.h"
#include <QFileDialog>
#include <QPrinter>
#include <QPrintDialog>

DruckDialog::DruckDialog(const QList<int> &l, DialogTyp t, QWidget *parent) : QDialog(parent)
{
    //ui.progress->setVisible(false);
    if (t==dlgKunde) auftritt = false;
    else auftritt = true;
    liste = l;
    sequenz = 0;
    ui.setupUi(this);
    resize(800,600);

    if (auftritt) setWindowTitle("Auftritte drucken");
    else setWindowTitle("Kunden drucken");

    if (liste.count() > 1)
    {
        ui.cbPdf->setEnabled(false);
        ui.cbPdf->setCheckState(Qt::Unchecked);
    }

    QDir dir(ARBEITSVERZ + "vorlagen/");
    QStringList filter;
    filter << "*.zk";
    ui.vorlagen->addItems(dir.entryList(filter));
    ui.vorschau->setHtml("Bitte eine Vorlage ausw�hlen");
    connect(ui.btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui.btnVor, SIGNAL(clicked()), this, SLOT(weiter()));
    connect(ui.btnZurueck, SIGNAL(clicked()), this, SLOT(zurueck()));
    connect(ui.btnDrucken, SIGNAL(clicked()), this, SLOT(drucken()));
    connect(ui.vorlagen, SIGNAL(currentRowChanged(int)),
            this, SLOT(vorlageGeaendert(int)));
}

void DruckDialog::vorlageGeaendert(int)
{
    Kunde k = Datenbank::kunde(liste.at(sequenz));
    ui.vorschau->setHtml(generiereHtml());
}

void DruckDialog::weiter()
{
    if (sequenz < liste.count()-1) sequenz++;
    ui.vorschau->setHtml(generiereHtml());
}

void DruckDialog::zurueck()
{
    if (sequenz > 0) sequenz--;
    ui.vorschau->setHtml(generiereHtml());
}

QString DruckDialog::generiereHtml()
{
    ui.label->setText (QString::number(sequenz+1) + " von " + QString::number(liste.count()));
    QString datei = ARBEITSVERZ + "vorlagen/" + ui.vorlagen->currentItem()->text();
    if (auftritt)
    {
        Auftritt a = Datenbank::auftritt(liste.at(sequenz));
        return Datenbank::generiereHtml(datei,a);
    }
    else
    {
        Kunde k = Datenbank::kunde(liste.at(sequenz));
        return Datenbank::generiereHtml(datei,k);
    }
}

QString DruckDialog::getPfad()
{
    QString pfad = ARBEITSVERZ + "pdf/";
    QString doc = ui.vorlagen->currentItem()->text();
    doc.chop(3);    // 3 Zeichen abschneiden
    if (auftritt)
    {
        Auftritt a = Datenbank::auftritt(liste.at(sequenz));
        pfad += a.auftragseingang.toString("yyyy_MM_dd-");
        pfad += QString::number(a.auftrittnummer);
        pfad += "-" + doc;
    }
    else
    {
        Kunde k = Datenbank::kunde(liste.at(sequenz));
        pfad += k.name + "_" + k.vorname;
        pfad += pfad += "-" + doc;
    }
    pfad += ".pdf";

    return (pfad);
}

void DruckDialog::drucken()
{
    if (ui.cbPdf->checkState())
    {
        QString fname = QFileDialog::getSaveFileName (this, "Pdf speichern als ...", getPfad(), "*.pdf");
        if (fname != "")
        {
            QPrinter pdfPrinter;
            pdfPrinter.setOutputFormat(QPrinter::PdfFormat);
            pdfPrinter.setOutputFileName(fname);
            ui.vorschau->document()->print(&pdfPrinter);
        }
    }

    if (!ui.cbDruck->checkState()) return;
    QPrinter printer;
    QPrintDialog dlg(&printer,this);
    if (dlg.exec() == QDialog::Accepted)
    {
            for (sequenz=0; sequenz < liste.count(); sequenz++)
            {
                QPrinter printer;
                printer.setDocName("Zauberkuenstler" + QString::number(sequenz+1));
                ui.vorschau->setHtml(generiereHtml());
                ui.vorschau->document()->print(&printer);
            }
    }
}

