#include "Kalender.h"

Kalender::Kalender(QWidget *parent) : QCalendarWidget(parent)
{
	setFixedSize(400,300);
    setFirstDayOfWeek(Qt::Monday);
    //setGridVisible(true);
    setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
    showToday();
}

void Kalender::paintCell ( QPainter *p, const QRect & r, const QDate & d) const
{
    QCalendarWidget::paintCell(p,r,d);
    if (Datenbank::termine(d))
    {
        QColor c(255,255,0,100);
        QBrush b(c);
        p->fillRect(r,b);
    }
}


