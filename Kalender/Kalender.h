#ifndef KALENDER
#define KALENDER

#include "Datenbank/Datenbank.h"
#include <QCalendarWidget>
#include <QtGui>

class Kalender : public QCalendarWidget
{
	Q_OBJECT

    private slots:

    public slots:

    public:
        Kalender(QWidget *parent = 0);

    protected:
        virtual void paintCell ( QPainter *p, const QRect & r, const QDate & d) const;

};

#endif

