#ifndef DATENBANK
#define DATENBANK

#include <QtSql>
#include <QtCore>
#include <QMessageBox>

#define MWST 19.0

#define ARBEITSVERZ QDir::homePath() + "/Zauberkuenstler/"
#define dbName "zauberkuenstler.db"

typedef QList<int> t_kundenListe;
typedef QList<int> t_auftrittListe;

class Kunde
{
    public:
        int kundennummer;
        QString firma;
        QString anrede;
        QString vorname;
        QString name;
        QString strasse;
        QString plz;
        QString wohnort;
        QString telefon;
        QString mobil;
        QString fax;
        QString email;
        QString homepage;
        QString werbung;

};

class Auftritt
{
    public:
    int auftrittnummer;
    QDate auftragseingang;
    Kunde kunde;
    QDate datum;
    QTime uhrzeit;
    QString dauer;
    QString eintreffen;
    QString ort_name;
    QString ort_strasse;
    QString ort_plz;
    QString ort_ort;
    QString melden_bei;
    bool ueberraschung;
    QString veranstaltungsart;
    double gage;
    double fahrtgeld;
    double sonstiges;
    QString vereinbarung;
    QString anfahrt;
};

class Datenbank
{
    public:
    static bool verbinden();
    static bool erzeugen(Kunde &k);
    static bool erzeugen(Auftritt &a);
    static bool aendern(const Kunde &k);
    static bool aendern(const Auftritt &a);
    static bool loeschen(const Kunde &k);
    static bool loeschen(const Auftritt &a);
    static int termine(const QDate &d);
    static Kunde kunde(int kundennummer);
    static Auftritt auftritt(int auftrittnummer);
    static t_auftrittListe agenda(const QDate &d);
    static t_auftrittListe kundenAuftritte(int kundennummer);
    static t_kundenListe existiert(const Kunde &k);
    static t_kundenListe alleKunden(const QString &where = "");
    static t_auftrittListe alleAuftritte(const QString &where = "");
    static QString generiereHtml(const QString &datei, const Kunde &k);
    static QString generiereHtml(const QString &datei, const Auftritt &a);


};

#endif

