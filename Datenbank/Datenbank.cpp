#include "Datenbank.h"

bool Datenbank::verbinden()
{
    QDir dir(ARBEITSVERZ);
    if (!dir.exists())
    {
        QStringList args;
        dir.mkdir(ARBEITSVERZ);
        QMessageBox::about(NULL,"Datenbankfehler",
        "Das Arbeitsverzeichnis " + ARBEITSVERZ +
        " existierte nicht und wurde erzeugt.");
    }
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(ARBEITSVERZ + dbName);
    if (!db.open()) return(false);
    QSqlQuery query;
    query.prepare ( "CREATE TABLE IF NOT EXISTS kunden ("
                        "kundennummer   INTEGER PRIMARY KEY AUTOINCREMENT,"
                        "anrede         TEXT,"
                        "firma          TEXT,"
                        "name           TEXT,"
                        "vorname        TEXT,"
                        "strasse        TEXT,"
                        "plz            TEXT,"
                        "wohnort        TEXT,"
                        "telefon        TEXT,"
                        "mobil          TEXT,"
                        "fax            TEXT,"
                        "email          TEXT,"
                        "homepage       TEXT,"
                        "werbung        TEXT"
                    ");");
    if(!query.exec()) return(false);

    query.prepare ( "CREATE TABLE IF NOT EXISTS auftritt ("
                        "auftrittnummer     INTEGER PRIMARY KEY AUTOINCREMENT,"
                        "auftragseingang    TEXT,"
                        "kunde              INTEGER,"
                        "datum              TEXT,"
                        "uhrzeit            TEXT,"
                        "dauer              TEXT,"
                        "eintreffen         TEXT,"
                        "ort_name           TEXT,"
                        "ort_strasse        TEXT,"
                        "ort_plz            TEXT,"
                        "ort_ort            TEXT,"
                        "melden_bei         TEXT,"
                        "ueberraschung      INTEGER,"
                        "veranstaltungsart  TEXT,"
                        "gage               REAL,"
                        "fahrtgeld          REAL,"
                        "sonstiges          REAL,"
                        "vereinbarung       TEXT,"
                        "anfahrt            TEXT"
                    ");");
    return(query.exec());
}

bool Datenbank::erzeugen(Auftritt &a)
{
    QSqlQuery query;
	query.prepare(  "INSERT INTO auftritt (auftragseingang,kunde,"
                    "datum,uhrzeit,dauer,eintreffen,ort_name,"
                    "ort_strasse,ort_plz,ort_ort,melden_bei,ueberraschung,"
                    "veranstaltungsart,gage,fahrtgeld,sonstiges,"
                    "vereinbarung,anfahrt) "
                    "VALUES(:auftragseingang,:kunde,"
                    ":datum,:uhrzeit,:dauer,:eintreffen,:ort_name,"
                    ":ort_strasse,:ort_plz,:ort_ort,:melden_bei,:ueberraschung,"
                    ":veranstaltungsart,:gage,:fahrtgeld,:sonstiges,"
                    ":vereinbarung,:anfahrt)");
	query.bindValue(":auftragseingang", a.auftragseingang);
	query.bindValue(":kunde", a.kunde.kundennummer);
	query.bindValue(":datum", a.datum);
	query.bindValue(":uhrzeit", a.uhrzeit);
	query.bindValue(":dauer", a.dauer);
	query.bindValue(":eintreffen", a.eintreffen);
	query.bindValue(":ort_name", a.ort_name);
	query.bindValue(":ort_strasse", a.ort_strasse);
	query.bindValue(":ort_plz", a.ort_plz);
	query.bindValue(":ort_ort", a.ort_ort);
	query.bindValue(":melden_bei", a.melden_bei);
	query.bindValue(":ueberraschung", a.ueberraschung);
	query.bindValue(":veranstaltungsart", a.veranstaltungsart);
	query.bindValue(":gage", a.gage);
	query.bindValue(":fahrtgeld", a.fahrtgeld);
	query.bindValue(":sonstiges", a.sonstiges);
	query.bindValue(":vereinbarung", a.vereinbarung);
	query.bindValue(":anfahrt", a.anfahrt);

	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    query.prepare(  "SELECT seq FROM sqlite_sequence WHERE name='auftritt'");
	query.exec();
	query.next();
	QSqlRecord record = query.record();
	a.auftrittnummer = query.value(record.indexOf("seq")).toInt();
	return(status);
}

bool Datenbank::erzeugen(Kunde &k)
{
    QSqlQuery query;
	query.prepare(  "INSERT INTO kunden (anrede,firma,"
                    "name,vorname,strasse,plz,wohnort,"
                    "telefon,mobil,fax,email,homepage,werbung)"
                    "VALUES(:anrede,:firma,:name,"
                    ":vorname,:strasse,:plz,:wohnort,:telefon,:mobil,"
                    ":fax,:email,:homepage,:werbung);");
	query.bindValue(":anrede", k.anrede);
	query.bindValue(":firma", k.firma);
	query.bindValue(":name", k.name);
	query.bindValue(":vorname", k.vorname);
	query.bindValue(":strasse", k.strasse);
	query.bindValue(":plz", k.plz);
	query.bindValue(":wohnort", k.wohnort);
	query.bindValue(":telefon", k.telefon);
	query.bindValue(":mobil", k.mobil);
	query.bindValue(":fax", k.fax);
	query.bindValue(":email", k.email);
	query.bindValue(":homepage", k.homepage);
	query.bindValue(":werbung", k.werbung);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
	query.prepare(  "SELECT seq FROM sqlite_sequence WHERE name='kunden'");
	query.exec();
	query.next();
	QSqlRecord record = query.record();
	k.kundennummer = query.value(record.indexOf("seq")).toInt();
	return(status);
}


bool Datenbank::aendern(const Kunde &k)
{
    QSqlQuery query;
    query.prepare(  "UPDATE kunden SET anrede=:anrede,firma=:firma,"
                    "werbung=:werbung,name=:name,vorname=:vorname,"
                    "strasse=:strasse,plz=:plz,wohnort=:wohnort,telefon=:telefon,"
                    "mobil=:mobil,fax=:fax,email=:email,homepage=:homepage "
                    "WHERE kundennummer= :kundennummer;");

    query.bindValue(":kundennummer", k.kundennummer);
	query.bindValue(":anrede", k.anrede);
	query.bindValue(":firma", k.firma);
	query.bindValue(":name", k.name);
	query.bindValue(":vorname", k.vorname);
	query.bindValue(":strasse", k.strasse);
	query.bindValue(":plz", k.plz);
	query.bindValue(":wohnort", k.wohnort);
	query.bindValue(":telefon", k.telefon);
	query.bindValue(":mobil", k.mobil);
	query.bindValue(":fax", k.fax);
	query.bindValue(":email", k.email);
	query.bindValue(":homepage", k.homepage);
	query.bindValue(":werbung", k.werbung);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
	return(status);
}


bool Datenbank::aendern(const Auftritt &a)
{
    QSqlQuery query;
    query.prepare(  "UPDATE auftritt SET "
                    "auftragseingang=:auftragseingang, kunde=:kunde,"
                    "datum=:datum,uhrzeit=:uhrzeit,dauer=:dauer,"
                    "eintreffen=:eintreffen,ort_name=:ort_name,"
                    "ort_strasse=:ort_strasse,ort_plz=:ort_plz,"
                    "ort_ort=:ort_ort,melden_bei=:melden_bei,"
                    "ueberraschung=:ueberraschung,"
                    "veranstaltungsart=:veranstaltungsart,"
                    "gage=:gage,fahrtgeld=:fahrtgeld,sonstiges=:sonstiges,"
                    "vereinbarung=:vereinbarung,anfahrt=:anfahrt "
                    "WHERE auftrittnummer==:auftrittnummer;");

    query.bindValue(":auftrittnummer", a.auftrittnummer);
    query.bindValue(":auftragseingang", a.auftragseingang);
	query.bindValue(":kunde", a.kunde.kundennummer);
	query.bindValue(":datum", a.datum);
	query.bindValue(":uhrzeit", a.uhrzeit);
	query.bindValue(":dauer", a.dauer);
	query.bindValue(":eintreffen", a.eintreffen);
	query.bindValue(":ort_name", a.ort_name);
	query.bindValue(":ort_strasse", a.ort_strasse);
	query.bindValue(":ort_plz", a.ort_plz);
	query.bindValue(":ort_ort", a.ort_ort);
	query.bindValue(":melden_bei", a.melden_bei);
	query.bindValue(":ueberraschung", a.ueberraschung);
	query.bindValue(":veranstaltungsart", a.veranstaltungsart);
	query.bindValue(":gage", a.gage);
	query.bindValue(":fahrtgeld", a.fahrtgeld);
	query.bindValue(":sonstiges", a.sonstiges);
	query.bindValue(":vereinbarung", a.vereinbarung);
	query.bindValue(":anfahrt", a.anfahrt);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text()+query.lastQuery());
	return(status);
}

bool Datenbank::loeschen(const Kunde &k)
{
    QSqlQuery query;

    query.prepare(  "SELECT kunde FROM auftritt "
                    "WHERE kunde= :kundennummer;");
    query.bindValue(":kundennummer", k.kundennummer);
    query.exec();
    if (query.next())
    {
        QMessageBox::about(NULL,"Datenbankfehler", "Kunde hat noch Auftritte");
        return (false);
    }
    query.prepare(  "DELETE FROM kunden "
                    "WHERE kundennummer= :kundennummer;");
    query.bindValue(":kundennummer", k.kundennummer);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
	return(status);
}

bool Datenbank::loeschen(const Auftritt &a)
{
    QSqlQuery query;
    query.prepare(  "DELETE FROM auftritt "
                    "WHERE auftrittnummer==:auftrittnummer;");
    query.bindValue(":auftrittnummer", a.auftrittnummer);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
	return(status);
}

Kunde Datenbank::kunde(int kundennummer)
{
    QSqlQuery query;
    query.prepare ("SELECT * FROM kunden WHERE kundennummer==:kundennummer");
    query.bindValue(":kundennummer", kundennummer);
    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    query.next();
    QSqlRecord record = query.record();
    Kunde k;
    k.kundennummer = query.value(record.indexOf("kundennummer")).toInt();
    k.anrede  = query.value(record.indexOf("anrede")).toString();
	k.firma   = query.value(record.indexOf("firma")).toString();
	k.name    = query.value(record.indexOf("name")).toString();
	k.vorname = query.value(record.indexOf("vorname")).toString();
	k.strasse = query.value(record.indexOf("strasse")).toString();
	k.plz     = query.value(record.indexOf("plz")).toString();
	k.wohnort = query.value(record.indexOf("wohnort")).toString();
	k.telefon = query.value(record.indexOf("telefon")).toString();
	k.mobil   = query.value(record.indexOf("mobil")).toString();
	k.fax     = query.value(record.indexOf("fax")).toString();
	k.email   = query.value(record.indexOf("email")).toString();
	k.homepage= query.value(record.indexOf("homepage")).toString();
	k.werbung = query.value(record.indexOf("werbung")).toString();
    return (k);
}

Auftritt Datenbank::auftritt(int auftrittnummer)
{
    QSqlQuery query;
    query.prepare ("SELECT * FROM auftritt WHERE auftrittnummer==:auftrittnummer");
    query.bindValue(":auftrittnummer", auftrittnummer);
    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    query.next();
    QSqlRecord record = query.record();

    Auftritt a;
    a.auftrittnummer = query.value(record.indexOf("auftrittnummer")).toInt();

    int kundennummer = query.value(record.indexOf("kunde")).toInt();
    a.kunde = kunde(kundennummer);

    a.auftragseingang = query.value(record.indexOf("auftragseingang")).toDate();
    a.datum = query.value(record.indexOf("datum")).toDate();
    a.uhrzeit = query.value(record.indexOf("uhrzeit")).toTime();
    a.dauer = query.value(record.indexOf("dauer")).toString();
    a.eintreffen = query.value(record.indexOf("eintreffen")).toString();
    a.ort_name = query.value(record.indexOf("ort_name")).toString();
    a.ort_strasse = query.value(record.indexOf("ort_strasse")).toString();
    a.ort_plz = query.value(record.indexOf("ort_plz")).toString();
    a.ort_ort = query.value(record.indexOf("ort_ort")).toString();
    a.melden_bei = query.value(record.indexOf("melden_bei")).toString();
    a.ueberraschung = query.value(record.indexOf("ueberraschung")).toBool();
    a.veranstaltungsart = query.value(record.indexOf("veranstaltungsart")).toString();
    a.gage = query.value(record.indexOf("gage")).toDouble();
    a.fahrtgeld = query.value(record.indexOf("fahrtgeld")).toDouble();
    a.sonstiges = query.value(record.indexOf("sonstiges")).toDouble();
    a.vereinbarung = query.value(record.indexOf("vereinbarung")).toString();
    a.anfahrt = query.value(record.indexOf("anfahrt")).toString();

    return (a);
}

t_kundenListe Datenbank::existiert(const Kunde &k)
{
    t_kundenListe Liste;
    QSqlQuery query;
    query.prepare ( "SELECT kundennummer FROM kunden WHERE "
                    "LOWER(firma) LIKE LOWER(:firma) "
                    "AND LOWER(vorname) LIKE LOWER(:vorname) "
                    "AND LOWER(name) LIKE LOWER(:name)");
    query.bindValue(":firma", k.firma + "%");
	query.bindValue(":name", k.name + "%");
	query.bindValue(":vorname", k.vorname + "%");
    bool status = query.exec();
    if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    QSqlRecord record = query.record();
    while(query.next())
        Liste.append(query.value(record.indexOf("kundennummer")).toInt());
    return(Liste);
}

t_kundenListe Datenbank::alleKunden(const QString &where)
{
    t_kundenListe Liste;
    QSqlQuery query;
    QString filter;
    if (where != "")
        filter = "WHERE " + where;
    query.prepare ( "SELECT kundennummer FROM kunden " + filter +
                    "ORDER BY (UPPER(firma) || UPPER(name) || UPPER(vorname))");
    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    QSqlRecord record = query.record();
    while(query.next())
        Liste.append(query.value(record.indexOf("kundennummer")).toInt());
    return(Liste);
}

t_auftrittListe Datenbank::alleAuftritte(const QString &where)
{
    t_auftrittListe Liste;
    QSqlQuery query;
    QString filter;
    if (where != "")
        filter = "AND " + where;
    query.prepare ( "SELECT auftrittnummer FROM auftritt, kunden "
                    "WHERE kunde == kundennummer " + filter +
                    "ORDER BY (DATE(datum) || TIME(uhrzeit))");

    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    QSqlRecord record = query.record();
    while(query.next())
        Liste.append(query.value(record.indexOf("auftrittnummer")).toInt());
    return(Liste);
}

t_auftrittListe Datenbank::agenda(const QDate &d)
{
    t_auftrittListe Liste;
    QSqlQuery query;
    query.prepare ( "SELECT auftrittnummer FROM auftritt "
                    "WHERE DATE(datum)==DATE(:datum) "
                    "ORDER BY TIME(uhrzeit)");
    query.bindValue(":datum", d);
    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    QSqlRecord record = query.record();
    while(query.next())
        Liste.append(query.value(record.indexOf("auftrittnummer")).toInt());
    return(Liste);
}

t_auftrittListe Datenbank::kundenAuftritte(int kundennummer)
{
    t_auftrittListe Liste;
    QSqlQuery query;
    query.prepare ( "SELECT auftrittnummer FROM auftritt "
                    "WHERE kunde == :kundennummer "
                    "ORDER BY DATE(datum)");
    query.bindValue(":kundennummer", kundennummer);
    bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    QSqlRecord record = query.record();
    while(query.next())
        Liste.append(query.value(record.indexOf("auftrittnummer")).toInt());
    return(Liste);
}

int Datenbank::termine(const QDate &d)
{
    int i = 0;
    QSqlQuery query;
    query.prepare(  "SELECT datum FROM auftritt "
                    "WHERE DATE(datum)==DATE(:datum);");
    query.bindValue(":datum", d);
	bool status = query.exec();
	if (!status)
        QMessageBox::about(NULL,"Datenbankfehler", query.lastError().text());
    while (query.next()) i++;
    return (i);
}

QString Datenbank::generiereHtml(const QString &datei, const Kunde &k)
{
        QFile f(datei);
        if (!f.open(QIODevice::ReadOnly))
            QMessageBox::about( NULL,"Dateifehler",
                                "Kann Datei " + datei + " nicht finden !");
        QByteArray array = f.readAll();
        QString html;
        html.append(array);
        html.replace("src=\"", "src=\"" + ARBEITSVERZ + "vorlagen/bilder/");
		html.replace("[heute]",  QDate::currentDate().toString("dd.MM.yyyy"));
        html.replace("[kundennummer]", QString::number(k.kundennummer));
        html.replace("[firma]", k.firma);
        html.replace("[anrede]", k.anrede);
        html.replace("[vorname]", k.vorname);
        html.replace("[name]", k.name);
        html.replace("[strasse]", k.strasse);
        html.replace("[plz]", k.plz);
        html.replace("[wohnort]", k.wohnort);
        html.replace("[telefon]", k.telefon);
        html.replace("[mobil]", k.mobil);
        html.replace("[fax]", k.fax);
        html.replace("[email]", k.email);
        html.replace("[homepage]", k.homepage);
        html.replace("[werbung]", k.werbung);
        return html;
}

QString Datenbank::generiereHtml(const QString &datei, const Auftritt &a)
{
        QFile f(datei);
        if (!f.open(QIODevice::ReadOnly))
            QMessageBox::about( NULL,"Dateifehler",
                                "Kann Datei " + datei + " nicht finden !");
        QByteArray array = f.readAll();
        QString html;
        html.append(array);
		html.replace("src=\"", "src=\"" + ARBEITSVERZ + "vorlagen/bilder/");
        html.replace("[heute]",  QDate::currentDate().toString("dd.MM.yyyy"));
        html.replace("[kundennummer]", QString::number(a.kunde.kundennummer));
        html.replace("[firma]", a.kunde.firma);
        html.replace("[anrede]", a.kunde.anrede);
        html.replace("[vorname]", a.kunde.vorname);
        html.replace("[name]", a.kunde.name);
        html.replace("[strasse]", a.kunde.strasse);
        html.replace("[plz]", a.kunde.plz);
        html.replace("[wohnort]", a.kunde.wohnort);
        html.replace("[telefon]", a.kunde.telefon);
        html.replace("[mobil]", a.kunde.mobil);
        html.replace("[fax]", a.kunde.fax);
        html.replace("[email]", a.kunde.email);
        html.replace("[homepage]", a.kunde.homepage);
        html.replace("[werbung]", a.kunde.werbung);
        html.replace("[auftrittnummer]", QString::number(a.auftrittnummer));
        html.replace("[auftragseingang]", a.auftragseingang.toString("dddd dd.MM.yyyy"));
        html.replace("[auftrittid]", a.auftragseingang.toString("yyyy_MM_dd-")
                                    + QString::number(a.auftrittnummer));
        html.replace("[datum]", a.datum.toString("dddd dd.MM.yyyy"));
        html.replace("[uhrzeit]", a.uhrzeit.toString("hh:mm"));
        html.replace("[dauer]", a.dauer);
        html.replace("[eintreffen]", a.eintreffen);
        html.replace("[ort_name]", a.ort_name);
        html.replace("[ort_strasse]", a.ort_strasse);
        html.replace("[ort_plz]", a.ort_plz);
        html.replace("[ort_ort]", a.ort_ort);
        html.replace("[melden_bei]", a.melden_bei);
        html.replace("[veranstaltungsart]", a.veranstaltungsart);

        html.replace("[gage]", QString::number(a.gage,'f',2));
        html.replace("[fahrtgeld]", QString::number(a.fahrtgeld,'f',2));
        html.replace("[sonstiges]", QString::number(a.sonstiges,'f',2));

        double gesamt = a.gage + a.fahrtgeld + a.sonstiges;
        double mwst = (gesamt/100.0) * (100.0 + MWST);

        html.replace("[mwst]", QString::number(MWST,'f',2));
        html.replace("[gesamt]", QString::number(gesamt,'f',2));
        html.replace("[gesamt+mwst]", QString::number(mwst,'f',2));

        html.replace("[vereinbarung]", a.vereinbarung);
        html.replace("[anfahrt]", a.anfahrt);

        if (a.ueberraschung)
            html.replace("[ueberraschung]", "Ja");
        else
            html.replace("[ueberraschung]", "Nein");
        return html;
}



