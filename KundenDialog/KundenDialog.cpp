#include "KundenDialog.h"

KundenDialog::KundenDialog(QWidget *parent) : QDialog(parent)
{
    resize(500,600);
    setWindowTitle("Neuen Kunden anlegen");
    topLayout = new QVBoxLayout(this);
    buttonLayout = new QHBoxLayout(this);
    maske = new KundenMaske(this);
    btnSpeichern = new QPushButton("Kunde anlegen");
    btnAbbrechen = new QPushButton("Abbrechen");
    topLayout->addWidget(maske);
    topLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(btnAbbrechen);
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnSpeichern);

    connect(btnSpeichern, SIGNAL(clicked()), this, SLOT(anlegen()));
    connect(btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
}

KundenDialog::KundenDialog(int kundennummer, QWidget *parent) : QDialog(parent)
{
    resize(500,600);
    setWindowTitle("Kunde bearbeiten");
    this->kundennummer = kundennummer;
    topLayout = new QVBoxLayout(this);
    buttonLayout = new QHBoxLayout(this);
    maske = new KundenMaske(this);
    btnSpeichern = new QPushButton(QLatin1String("Kunde �ndern"));
    btnLoeschen = new QPushButton(QLatin1String("Kunde l�schen"));
    btnAbbrechen = new QPushButton("Abbrechen");
    btnDrucken = new QPushButton("Drucken");
    topLayout->addWidget(maske);
    topLayout->addLayout(buttonLayout);
    buttonLayout->addWidget(btnAbbrechen);
    buttonLayout->addStretch();
    buttonLayout->addWidget(btnSpeichern);
    buttonLayout->addWidget(btnLoeschen);
    buttonLayout->addWidget(btnDrucken);
    Kunde k;
    k = Datenbank::kunde(kundennummer);
    maske->setKunde(k);

    connect(btnDrucken, SIGNAL(clicked()), this, SLOT(drucken()));
    connect(btnSpeichern, SIGNAL(clicked()), this, SLOT(aendern()));
    connect(btnAbbrechen, SIGNAL(clicked()), this, SLOT(close()));
    connect(btnLoeschen, SIGNAL(clicked()), this, SLOT(loeschen()));
}

void KundenDialog::anlegen()
{
    Kunde k = maske->getKunde();
    Datenbank::erzeugen(k);
    close();
}

void KundenDialog::aendern()
{
    Kunde k = maske->getKunde();
    k.kundennummer = kundennummer;
    Datenbank::aendern(k);
    close();
}

void KundenDialog::drucken()
{
    t_kundenListe Liste;
    Liste.append(this->kundennummer);
    //Liste = Datenbank::alleKunden();
    DruckDialog dlg(Liste,DruckDialog::dlgKunde,this);
    dlg.exec();
}


void KundenDialog::loeschen()
{
        //TODO: Deutsche Schaltf�chen //
        int reply =QMessageBox::warning(this,  QLatin1String("Kunde l�schen"),
                                QLatin1String("wollen Sie diesen Kunden wirklich l�schen ?"),
                                QMessageBox::No | QMessageBox::Yes);

        if (reply == QMessageBox::Yes)
        {
            Kunde k = maske->getKunde();
            k.kundennummer = kundennummer;
            Datenbank::loeschen(k);
        }

        close();
}
