#include "KundenMaske.h"

KundenMaske::KundenMaske(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);
}

Kunde KundenMaske::getKunde()
{
        Kunde k;
        k.firma = ui.leFirma->displayText();
        k.name = ui.leName->displayText();
        k.vorname = ui.leVorname->displayText();
        k.strasse = ui.leStrasse->displayText();
        k.plz = ui.lePlz->displayText();
        k.wohnort = ui.leWohnort->displayText();
        k.telefon = ui.leTelefon->displayText();
        k.mobil = ui.leMobil->displayText();
        k.fax = ui.leFax->displayText();
        k.email = ui.leEmail->displayText();
        k.homepage = ui.leHomepage->displayText();

        k.anrede = ui.cmbAnrede->currentText();
        k.werbung = ui.cmbWerbung->currentText();
        return(k);
}

void KundenMaske::setKunde(Kunde k)
{
        ui.leFirma->setText(k.firma);
        ui.leName->setText(k.name);
        ui.leVorname->setText(k.vorname);
        ui.leStrasse->setText(k.strasse);
        ui.lePlz->setText(k.plz);
        ui.leWohnort->setText(k.wohnort);
        ui.leTelefon->setText(k.telefon);
        ui.leMobil->setText(k.mobil);
        ui.leFax->setText(k.fax);
        ui.leEmail->setText(k.email);
        ui.leHomepage->setText(k.homepage);

        ui.cmbAnrede->setCurrentIndex(ui.cmbAnrede->findText(k.anrede));
        ui.cmbWerbung->setCurrentIndex(ui.cmbWerbung->findText(k.werbung));
}

void KundenMaske::editierbar(bool b)
{
        ui.leFirma->setEnabled(b);
        ui.leName->setEnabled(b);
        ui.leVorname->setEnabled(b);
        ui.leStrasse->setEnabled(b);
        ui.lePlz->setEnabled(b);
        ui.leWohnort->setEnabled(b);
        ui.leTelefon->setEnabled(b);
        ui.leMobil->setEnabled(b);
        ui.leFax->setEnabled(b);
        ui.leEmail->setEnabled(b);
        ui.leHomepage->setEnabled(b);
        ui.cmbAnrede->setEnabled(b);
        ui.cmbWerbung->setEnabled(b);
}

