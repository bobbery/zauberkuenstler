#ifndef KUNDENMASKE
#define KUNDENMASKE

#include "ui_KundenMaske.h"
#include "Datenbank/Datenbank.h"

class KundenMaske : public QWidget
{
	Q_OBJECT

    private slots:

    public slots:
        void setKunde(Kunde k);

    public:
        KundenMaske(QWidget *parent = 0);
        Kunde getKunde();
        void editierbar(bool b);

    private:
        Ui::KundenMaske ui;
};

#endif

