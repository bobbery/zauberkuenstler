#ifndef KUNDENDIALOG
#define KUNDENDIALOG

#include <QDialog>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMessageBox>
#include "KundenMaske.h"
#include "Datenbank/Datenbank.h"
#include "DruckDialog/DruckDialog.h"

class KundenDialog : public QDialog
{
	Q_OBJECT

    private slots:
        void anlegen();
        void aendern();
        void loeschen();
        void drucken();

    public slots:

    public:
        KundenDialog(QWidget *parent = 0);
        KundenDialog(int kundennummer, QWidget *parent = 0);

    private:
        int kundennummer;
        QVBoxLayout * topLayout;
        KundenMaske * maske;
        QHBoxLayout * buttonLayout;
        QPushButton * btnSpeichern;
        QPushButton * btnAbbrechen;
        QPushButton * btnLoeschen;
        QPushButton * btnDrucken;

};

#endif


