#include "Hauptfenster.h"

Hauptfenster::Hauptfenster(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);
	auftrittLayout = new QHBoxLayout(ui.tabAuftritte);
	kundenLayout = new QHBoxLayout(ui.tabKunden);
	kundenLayoutRechts = new QVBoxLayout();
	auftrittLayoutLinks = new QVBoxLayout();
	kalender = new Kalender(ui.tabAuftritte);
	QPixmap pix(ARBEITSVERZ + "vorlagen/bilder/banner.png");
	logo = new QLabel();
	logo->setPixmap(pix);
	logo->setAlignment(Qt::AlignHCenter);
    agenda= new Agenda(ui.tabAuftritte);
	auftrittLayout->addLayout(auftrittLayoutLinks);
	auftrittLayoutLinks->addWidget(kalender);
	auftrittLayoutLinks->addWidget(logo);
	auftrittLayout->addWidget(agenda);
	kundenliste = new KundenListe(ui.tabKunden);
	kundenansicht = new KundenAnsicht(ui.tabKunden);
	auftrittliste = new AuftrittListe(ui.tabKunden);
	lblAuftritte = new QLabel("Auftritte dieses Kunden: ");
	kundenLayout->addWidget(kundenliste);
	kundenLayout->addLayout(kundenLayoutRechts);
	kundenLayoutRechts->addWidget(kundenansicht);
	kundenLayoutRechts->addWidget(lblAuftritte);
	kundenLayoutRechts->addWidget(auftrittliste);
    connect(ui.actionNeuer_Auftritt, SIGNAL(triggered(bool)), this, SLOT(neuerAuftritt()));
    connect(kalender, SIGNAL(activated(const QDate &)), this, SLOT(neuerAuftritt(const QDate &)));
    connect(kalender, SIGNAL(clicked(const QDate &)), agenda, SLOT(datumGeaendert(const QDate &)));
    connect(ui.actionNeuer_Kunde, SIGNAL(triggered(bool)), this, SLOT(neuerKunde()));
    connect(ui.actionDrucken, SIGNAL(triggered(bool)), this, SLOT(seriendruck()));
    connect(ui.actionBeenden, SIGNAL(triggered(bool)), qApp, SLOT(quit()));
    connect(kundenliste, SIGNAL(kundeGeaendert(int)), kundenansicht, SLOT(kundeGeaendert(int)));
    connect(kundenliste, SIGNAL(kundeGeaendert(int)), auftrittliste, SLOT(setKundennummer(int)));
	kundenliste->start();
}

void Hauptfenster::neuerAuftritt()
{
    AuftrittDialog dlg(QDate::currentDate(),this);
    dlg.exec();
    kundenliste->update();
}

void Hauptfenster::neuerAuftritt(const QDate &d)
{
    AuftrittDialog dlg(d,this);
    dlg.exec();
    agenda->datumGeaendert(d);
    kundenliste->update();
}

void Hauptfenster::neuerKunde()
{
    KundenDialog dlg(this);
    dlg.exec();
    kundenliste->update();
}

void Hauptfenster::seriendruck()
{
    SerienDruck dlg(this);
    dlg.exec();
}


