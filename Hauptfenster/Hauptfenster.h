#ifndef HAUPTFENSTER
#define HAUPTFENSTER

#include "ui_Hauptfenster.h"
#include "Datenbank/Datenbank.h"
#include "KundenDialog/KundenDialog.h"
#include "KundenAnsicht/KundenListe.h"
#include "KundenAnsicht/KundenAnsicht.h"
#include "KundenAnsicht/AuftrittListe.h"
#include "AuftrittDialog/AuftrittDialog.h"
#include "Kalender/Kalender.h"
#include "SerienDruck/SerienDruck.h"
#include "Agenda/Agenda.h"
#include <QHBoxLayout>

class Hauptfenster : public QMainWindow
{
	Q_OBJECT

    private slots:

    public slots:
        void neuerAuftritt();
        void neuerAuftritt(const QDate &d);
        void neuerKunde();
        void seriendruck();

    public:
        Hauptfenster(QWidget *parent = 0);

    private:
        Ui::Hauptfenster ui;
        QHBoxLayout * auftrittLayout;
		QVBoxLayout * auftrittLayoutLinks;
		QHBoxLayout * kundenLayout;
        QVBoxLayout * kundenLayoutRechts;

        Kalender * kalender;
		QLabel * logo;
        Agenda * agenda;
        KundenListe * kundenliste;
        KundenAnsicht * kundenansicht;
        QLabel * lblAuftritte;
        AuftrittListe * auftrittliste;
};

#endif



