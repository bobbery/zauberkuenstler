#include "Hauptfenster/Hauptfenster.h"
#include "Datenbank/Datenbank.h"
#include <QApplication>
#include <QProxyStyle>



class MyProxyStyle : public QProxyStyle
{
public:
    int styleHint(StyleHint hint, const QStyleOption *option = 0,
    const QWidget *widget = 0, QStyleHintReturn *returnData = 0) const {
        if (hint == QStyle:: SH_ItemView_ActivateItemOnSingleClick)
        return 0;
        return QProxyStyle::styleHint(hint, option, widget, returnData);
    }
};


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setStyle(new MyProxyStyle);

    Datenbank::verbinden();

    Hauptfenster hf;

    hf.show();

    app.exec();
}


